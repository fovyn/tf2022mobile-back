package be.bstorm.formation.tfmobile2022.repositories

import be.bstorm.formation.tfmobile2022.models.entities.Personne
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface PersonRepository: JpaRepository<Personne, Long> {
    @Query(value = "SELECT p FROM Personne p WHERE p.lastname = :lastname")
    fun findAllByLastname(@Param("lastname") lastname: String): List<Personne>
}