package be.bstorm.formation.tfmobile2022

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Tfmobile2022Application

fun main(args: Array<String>) {
	runApplication<Tfmobile2022Application>(*args)
}
