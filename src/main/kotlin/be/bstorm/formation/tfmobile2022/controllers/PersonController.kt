package be.bstorm.formation.tfmobile2022.controllers

import be.bstorm.formation.tfmobile2022.models.dtos.person.PersonCreate
import be.bstorm.formation.tfmobile2022.models.entities.Personne
import be.bstorm.formation.tfmobile2022.repositories.PersonRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.util.Optional

@RestController
class PersonController @Autowired constructor (
    private val personRepository: PersonRepository
) {

//    private val personnes: ArrayList<Personne> = arrayListOf(
//        Personne(1L, "Ovyn", "Flavian", LocalDate.of(1991, 7, 19)),
//        Personne(2L, "Ovyn", "Flavian", LocalDate.of(1991, 7, 19)),
//        Personne(3L, "Ovyn", "Flavian", LocalDate.of(1991, 7, 19)),
//        Personne(4L, "Ovyn", "Flavian", LocalDate.of(1991, 7, 19)),
//        Personne(5L, "Ovyn", "Flavian", LocalDate.of(1991, 7, 19)),
//        Personne(6L, "Ovyn", "Flavian", LocalDate.of(1991, 7, 19)),
//        Personne(7L, "Ovyn", "Flavian", LocalDate.of(1991, 7, 19)),
//        Personne(8L, "Ovyn", "Flavian", LocalDate.of(1991, 7, 19)),
//        Personne(9L, "Ovyn", "Flavian", LocalDate.of(1991, 7, 19)),
//        Personne(10L, "Ovyn", "Flavian", LocalDate.of(1991, 7, 19))
//    )

    @GetMapping(path = ["/person"])
    fun getAll(): List<Personne> {
        return this.personRepository.findAllByLastname("Blop");
    }

    @GetMapping(path = ["/person/{id:[0-9]+}"])
    fun getOne(@PathVariable() id: Long): Optional<Personne> {
        return this.personRepository.findById(id);
    }

//    @GetMapping(path = ["/person/{id:[0-9]+}/roles)

    @PostMapping(path = ["/person"])
    fun create(@RequestBody dto: PersonCreate): Personne {
        dto.let {
            val p = Personne();
            p.firstname = it.firstname;
            p.lastname = it.lastname;
            it.birthDate?.apply { p.birthDate = it.birthDate }

            return this.personRepository.save(p)
        }
    }

//    @PutMapping(path = ["/person/{id:[0-9]+}"])
//    fun update(@PathVariable id: Long, @RequestBody person: Personne): Personne {
//        // region Récupération en DB
//        var toUpdate: Personne = this.personnes.stream()
//            .filter { it.id == id }
//            .findFirst().get();
//        val index = this.personnes.indexOf(toUpdate);
//        // endregion
        // Mise à jour à partir de person de la requête
//        toUpdate.firstname = person.firstname
//        toUpdate.lastname = person.lastname
//        toUpdate.birthDate = person.birthDate

//        this.personnes[index] = toUpdate;

//        return toUpdate;
//    }

//    @PatchMapping(path = ["/person/{id:[0-9]+}/lastname"])
//    fun updateName(@PathVariable)

//    @DeleteMapping(path = ["/person/{id:[0-9]+}"])
//    fun remove(@PathVariable() id: Long): ResponseEntity<Unit> {
//        val toRemove = this.personnes.find { it.id == id };
//        val index = this.personnes.indexOf(toRemove);
//        this.personnes.removeAt(index);
//
//        return ResponseEntity.status(HttpStatus.ACCEPTED).body(null);
//    }
}