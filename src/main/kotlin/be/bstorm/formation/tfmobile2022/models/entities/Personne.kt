package be.bstorm.formation.tfmobile2022.models.entities

import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name= "PERSON")
data class Personne(

    var lastname: String,
    var firstname: String,
    var birthDate: LocalDate
) {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    var id: Long? = null;
    constructor() : this("", "", LocalDate.now()) {

    }
}