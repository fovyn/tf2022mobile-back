package be.bstorm.formation.tfmobile2022.models.dtos.person

import java.time.LocalDate

data class PersonCreate(
    val firstname: String,
    val lastname: String,
    val birthDate: LocalDate?
) {
}